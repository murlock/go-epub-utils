package main

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestInvalidURIElasticSearch(t *testing.T) {
	var err error

	push := new(elastic)
	err = push.init("BAD URL")
	assert.NotEqual(t, nil, err)
}

func launchFakeElastic(t *testing.T, called *int, books *[]*Book) *httptest.Server {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			w.WriteHeader(500)
			t.Error("Unsupported Method")
			return
		}

		/* TODO: check Content-Type is application/json" */
		if r.Header.Get("Content-Type") != "application/json" {
			w.WriteHeader(500)
			t.Error("Invalid Content-Type")
			return
		}

		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(500)
			t.Error("Failed to read  body")
			return
		}

		/* bulk mode */
		data = bytes.TrimSuffix(data, []byte("\x0a"))
		items := bytes.Split(data, []byte("\n"))

		/* following ones contains a document per line */
		idx := 0
		for {
			/* first line is index */
			idx++
			if idx == len(items) {
				w.WriteHeader(500)
				t.Errorf("Invalid number of items")
			}
			/* second one is book entry */
			var book = new(Book)
			if err = json.Unmarshal(items[idx], &book); err != nil {
				w.WriteHeader(500)
				t.Errorf("Failed to unmarshall book: %s", data)
				return
			}
			*books = append(*books, book)
			idx++
			if idx == len(items) {
				break
			}
		}

		*called += 1
		w.WriteHeader(201)
	}))
	return ts
}

func TestElastic(t *testing.T) {
	called := 0
	books := make([]*Book, 0)

	ts := launchFakeElastic(t, &called, &books)
	defer ts.Close()

	push := new(elastic)
	if err := push.init(ts.URL); err != nil {
		assert.Equal(t, nil, err)
	}
	push.start()
	var err error
	var book = createBook()

	err = push.push(book)
	push.stop()
	/* */
	assert.Equal(t, nil, err)
	assert.Equal(t, 1, called)
	assert.Equal(t, 1, len(books))
	assert.Equal(t, false, push.hasError)
}

func TestElasticBulk(t *testing.T) {
	called := 0
	books := make([]*Book, 0)

	ts := launchFakeElastic(t, &called, &books)
	defer ts.Close()
	push := new(elastic)
	if err := push.init(ts.URL); err != nil {
		assert.Equal(t, nil, err)
	}
	push.setBulkSize(2)

	push.start()
	var err error

	// push bulkSize books
	var book = createBookWithParams("1")
	err = push.push(book)
	assert.Equal(t, nil, err)
	book = createBookWithParams("2")
	err = push.push(book)
	assert.Equal(t, nil, err)

	// and new one
	book = createBookWithParams("3")
	err = push.push(book)
	assert.Equal(t, nil, err)

	push.stop()
	assert.Equal(t, 3, len(books))
	assert.Equal(t, "anonymous 1", books[0].Author)
	assert.Equal(t, "anonymous 2", books[1].Author)
	assert.Equal(t, "anonymous 3", books[2].Author)
	assert.Equal(t, 2, called)
	assert.Equal(t, false, push.hasError)
}

func TestElasticError(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		/* simulate an error */
		w.WriteHeader(500)
	}))
	defer ts.Close()

	push := new(elastic)
	if err := push.init(ts.URL); err != nil {
		assert.Equal(t, nil, err)
	}
	push.start()
	var book = createBook()
	err := push.push(book)
	assert.Equal(t, nil, err)
	push.stop()

	assert.Equal(t, true, push.hasError)
}
