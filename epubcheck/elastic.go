package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"sync"
)

type elastic struct {
	client   *http.Client
	url      string
	events   chan *Book
	wg       sync.WaitGroup
	bulkSize int
	hasError bool
}

/*
type notifier interface {
	init(url string) error
	start()
	push(book Book) error
	stop()
}
*/

type bulk_response struct {
	Took   int  `json:"took"`
	Errors bool `json:"errors"`
}

func (instance *elastic) init(rawurl string) error {
	if _, err := url.ParseRequestURI(rawurl); err != nil {
		return err
	}
	instance.client = &http.Client{}
	if len(rawurl) > 0 && !strings.HasSuffix(rawurl, "/") {
		rawurl += "/"
	}
	rawurl += "_bulk"
	instance.url = rawurl
	instance.bulkSize = 10
	instance.hasError = false
	return nil
}

func (instance *elastic) setBulkSize(count int) {
	instance.bulkSize = count
}

func (instance *elastic) start() {
	instance.wg.Add(1)
	instance.events = make(chan *Book, instance.bulkSize*2)
	go instance.consume()
}

func (instance *elastic) stop() {
	close(instance.events)
	instance.wg.Wait()
}

func (instance *elastic) consume() {
	var counter int
	buffer := strings.Builder{}
	buffer.Grow(2048 * instance.bulkSize)
	for {
		book, more := <-instance.events
		if more {
			counter += 1
			data, err := json.Marshal(book)
			if err == nil {
				_, err = buffer.WriteString(fmt.Sprintf("{\"index\": {\"_id\":\"%s\"}}\n", book.id))
			}
			if err == nil {
				_, err = buffer.Write(data)
			}
			if err == nil {
				buffer.WriteRune('\n')
			}
			if err == nil && counter >= instance.bulkSize {
				instance.post(buffer.String())
				buffer.Reset()
				counter = 0
			}
			if err != nil {
				/* reset buffer and counter */
				fmt.Println(err)
				buffer.Reset()
				counter = 0
			}
		} else {
			break
		}
	}
	if counter > 0 {
		instance.post(buffer.String())
	}
	instance.wg.Done()
}

func (instance *elastic) push(book *Book) error {
	instance.events <- book
	return nil
}

func (instance *elastic) post(buffer string) {
	req, err := http.NewRequest("POST", instance.url, strings.NewReader(buffer))
	if err != nil {
		fmt.Println(err)
		instance.hasError = true
		return
	}
	req.Header.Add("Content-Type", "application/json")
	resp, err := instance.client.Do(req)
	if err != nil {
		fmt.Println(err)
		instance.hasError = true
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode/100 != 2 {
		txt, _ := ioutil.ReadAll(resp.Body)
		fmt.Printf("Invalid status code (%d): %s\n", resp.StatusCode, string(txt))
		instance.hasError = true
		return
	}

	content, err := ioutil.ReadAll(resp.Body)
	var rep *bulk_response
	if err != nil {
		rep = new(bulk_response)
		err = json.Unmarshal(content, rep)
	}
	if err != nil {
		fmt.Fprintf(os.Stderr, "Errors detected to Elastic: %s", err)
		instance.hasError = true
	} else if rep != nil && rep.Errors {
		/* TODO: show errors encountered */
		fmt.Fprintf(os.Stderr, "Errors detected to Elastic")
		instance.hasError = true
	}
}
