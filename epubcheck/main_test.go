package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func createBook() *Book {
	return createBookWithParams("123")
}

func createBookWithParams(id string) *Book {
	var book = new(Book)
	book.id = id
	book.Author = "anonymous " + id
	book.Title = "title " + id
	return book
}

func TestUnexistingZip(t *testing.T) {
	var ret, _ = checkZip("examples/missing.epub", false)

	assert.Equal(t, false, ret, "Missing file not detected")
}

func TestInvalidZip(t *testing.T) {
	var ret, _ = checkZip("examples/invalid_compression.epub", false)

	assert.Equal(t, false, ret, "Invalid file not detected")
}

func TestValidZip(t *testing.T) {
	var ret, _ = checkZip("examples/skeleton.epub", false)

	assert.Equal(t, true, ret, "Valid file not detected")
}

func TestInvalidMetaInf(t *testing.T) {
	metainf := "invalid XML"

	var ret, err = parseMetaInf(strings.NewReader(metainf), uint64(len(metainf)))
	assert.Equal(t, "", ret)
	assert.NotNil(t, err)
}

func TestInvalidReadMetaInf(t *testing.T) {
	metainf := "invalid XML"

	var ret, err = parseMetaInf(strings.NewReader(metainf), uint64(len(metainf)*2))
	assert.Equal(t, "", ret)
	assert.NotNil(t, err)
}

func TestMissingEntryReadMetaInf(t *testing.T) {
	metainf := `
<?xml version="1.0" encoding="utf-8"?>
<container xmlns="urn:oasis:names:tc:opendocument:xmlns:container" version="1.0">
  <rootfiles>
    <rootfile full-path="OPS/content.opf" media-type="invalidmedia" />
  </rootfiles>
</container>
    `

	var ret, err = parseMetaInf(strings.NewReader(metainf), uint64(len(metainf)))
	assert.Equal(t, "", ret)
	assert.NotNil(t, err)
}

func TestValidMetaInf(t *testing.T) {
	metainf := `
<?xml version="1.0" encoding="utf-8"?>
<container xmlns="urn:oasis:names:tc:opendocument:xmlns:container" version="1.0">
  <rootfiles>
    <rootfile full-path="OPS/content.opf" media-type="application/oebps-package+xml" />
  </rootfiles>
</container>
    `

	var ret, err = parseMetaInf(strings.NewReader(metainf), uint64(len(metainf)))
	assert.Equal(t, "OPS/content.opf", ret)
	assert.Nil(t, err)
}

func TestInvalidBook(t *testing.T) {
	opts := new(Opts)

	var book, err = checkBook(opts, "examples/invalid_compression.epub")
	assert.Nil(t, book)
	assert.Equal(t, "OPF file not found", fmt.Sprint(err), "Invalid error")
}

func TestInvalidBookWithCheck(t *testing.T) {
	opts := new(Opts)
	opts.check = true

	book, err := checkBook(opts, "examples/invalid_compression.epub")
	assert.Nil(t, book)
	assert.Equal(t, "flate: corrupt input before offset 10", fmt.Sprint(err), "Invalid error")
}

func TestMissingBook(t *testing.T) {
	opts := new(Opts)
	var book, err = checkBook(opts, "examples/missing.epub")
	assert.Nil(t, book)
	assert.Equal(t, "open examples/missing.epub: no such file or directory", fmt.Sprint(err))
}

func TestValidBook(t *testing.T) {
	opts := new(Opts)
	var book, err = checkBook(opts, "examples/skeleton.epub")
	assert.Nil(t, err)
	assert.Equal(t, "Voyage au Centre de la Terre", book.Title, "Invalid title")
	assert.Equal(t, "Jules Verne", book.Author, "Invalid author")
}

func TestCsvExport(t *testing.T) {
	opts := new(Opts)
	opts.csv = true
	opts.tags = true
	/* TODO: add a parameter to capture csv ouput */
	csvWriter = initCsv(opts)

	var book, err = checkBook(opts, "examples/skeleton.epub")
	assert.Nil(t, err)
	assert.Equal(t, "Voyage au Centre de la Terre", book.Title, "Invalid title")
	assert.Equal(t, "Jules Verne", book.Author, "Invalid author")

	csvWriter.Flush()

	/* check csv ouput */
}

func TestBook(t *testing.T) {
	var book, err = parseXml("examples/skeleton.epub", false)
	assert.Nil(t, err, "Valid file throw an error !")
	assert.Equal(t, "Voyage au Centre de la Terre", book.Title, "Invalid title")
	assert.Equal(t, "Jules Verne", book.Author, "Invalid author")
}

func TestBookCreators(t *testing.T) {
	var book, err = parseXml("examples/creators.epub", false)

	assert.Nil(t, err, "Valid file throw an error !")
	assert.Equal(t, "Author", book.Author, "Invalid author")
}

func TestBookTags(t *testing.T) {
	var book, err = parseXml("examples/creators.epub", false)

	assert.Nil(t, err, "Valid file throw an error !")
	assert.Equal(t, 1, len(book.Tags))
	assert.Equal(t, "Science-fiction", book.Tags[0])
}

func TestStrictModeMissingMimeType(t *testing.T) {
	var book, err = parseXml("examples/creators.epub", true)

	assert.Nil(t, book)
	assert.NotNil(t, err)
	assert.Equal(t, "mimetype is missing", fmt.Sprint(err))
}

func TestStrictModeInvalidMimeType(t *testing.T) {
	var book, err = parseXml("examples/invalid_mimetype.epub", true)

	assert.Nil(t, book)
	assert.NotNil(t, err)
	assert.Equal(t, "Invalid content of mimetype", fmt.Sprint(err))
}

func TestParseDir(t *testing.T) {
	tests := parseDir("examples/")

	assert.Equal(t, 4, len(tests))
}

func TestCmdLine(t *testing.T) {
	var tests = []string{
		"--elastic", "http://localhost:9200/books",
		"--check",
		"--recurse",
		"--quiet",
		"/opt/eBooks/"}
	var opts = parseCmdLine(tests)

	assert.Equal(t, "http://localhost:9200/books", opts.elastic, "Elastic URL")
	assert.Equal(t, true, opts.check, "Check should be set")
	assert.Equal(t, true, opts.recurse, "Recurse should be set")
	assert.Equal(t, true, opts.quiet, "Quiet should be set")
	assert.Equal(t, 1, len(opts.nargs), "Should contains 1 arg")
}
