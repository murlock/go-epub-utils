package main

/* DONE:
   - check zip
   - check metadata content:
       - open content.opf to find META-INF/container.xml
       - open META-INF/container.xml
       - file META-INF/container.xml (should reference content.opf)
       - file content.opf
   - check content.opf data
       - author and title
*/
/* TODO:
   - use corountine to parse in // directories ?
   - check:
       - file mimetype at /
       - mimteype should be application/epub+zip
   - check content.opf data
       - languages
       - ...
   - check if epub is encrypted
   - add option to remove unwanted files:
       - iTunesArtwork
       - iTunesMetadata.plist
       - __MACOSX
       - META-INF/calibre_bookmarks.txt
*/

import (
	"archive/zip"
	"bytes"
	"crypto/md5"
	"encoding/csv"
	"encoding/xml"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	_ "reflect"
	"strings"
)

const MIMETYPE_FILE string = "mimetype"
const MIMETYPE_VALUE string = "application/epub+zip"
const CONTAINER_XML string = "META-INF/container.xml"
const OEBPS_MIMETYPE string = "application/oebps-package+xml"
const BUFSIZE = 5 * 1024 * 1024
const DEFAULT_OPF = "content.opf"
const DCELEMENTS_SPACE = "http://purl.org/dc/elements/1.1/"

/* found where it should be used */
// const OPENDOC_NS string = "urn:oasis:names:tc:opendocument:xmlns:container" (???)
// const OPF_SPACE = "http://www.idpf.org/2007/opf"

type Book struct {
	id        string
	Author    string   `json:"author"`
	Title     string   `json:"title"`
	Lang      string   `json:"lang,omitempty"`
	Filename  string   `json:"path"`
	Publisher string   `json:"publisher"`
	Tags      []string `json:"Tags"`
}

type mediaRootFile struct {
	Path  string `xml:"full-path,attr"`
	Media string `xml:"media-type,attr"`
}

type metaInfContainer struct {
	XMLName   xml.Name        `xml:"container"`
	Rootfiles []mediaRootFile `xml:"rootfiles>rootfile"`
}

type Opts struct {
	elastic           string
	elastic_bulk_size int
	help              bool
	check             bool
	strict            bool
	recurse           bool
	quiet             bool
	csv               bool
	tags              bool
	nargs             []string
}

var csvWriter *csv.Writer

var push *elastic

func checkZip(file string, strict bool) (bool, error) {
	var reader, err = zip.OpenReader(file)

	if err != nil {
		return false, err
	}

	buf := make([]byte, BUFSIZE)

	for _, entry := range reader.File {
		fdata, err := entry.Open()
		if err != nil {
			return false, err
		}

		data_to_read := entry.UncompressedSize64
		for {
			if data_to_read == 0 {
				break
			}
			nb := data_to_read
			if data_to_read > BUFSIZE {
				nb = BUFSIZE
			}
			n, err := io.ReadFull(fdata, buf)
			if uint64(n) != data_to_read && err != nil {
				return false, err
			}
			if uint64(n) != nb {
				return false, fmt.Errorf("Invalid size read")
			}
			data_to_read -= uint64(n)
			if strict {
				/* check if epub contains JS script */
				if bytes.Contains(buf, []byte("<script")) ||
					bytes.Contains(buf, []byte("javascript")) {
					return false, fmt.Errorf("Found script or javascript tag !")
				}
			}
		}

		fdata.Close()
	}

	reader.Close()
	return true, nil
}

func parseMetaInf(reader io.Reader, size uint64) (string, error) {
	buf := make([]byte, size)

	var metainf metaInfContainer
	_, err := io.ReadFull(reader, buf)

	if err == nil {
		err = xml.Unmarshal(buf, &metainf)
	}
	if err != nil {
		return "", err
	}

	for _, media := range metainf.Rootfiles {
		if media.Media == OEBPS_MIMETYPE {
			return media.Path, nil
		}
	}

	return "", fmt.Errorf("Invalid or no media available")
}

func parseXml(file string, strict bool) (*Book, error) {
	/* TODO: reuse checkZip code or handle */
	var zipreader, err = zip.OpenReader(file)
	if err != nil {
		return nil, err
	}

	defer zipreader.Close()

	/* we should parse META-INF/container.xml
	   but first version open directly content.opf */

	var book *Book = new(Book)
	book.Filename = file

	var field *string
	var field2 *[]string
	var reader io.Reader
	opf_file := false
	opf := DEFAULT_OPF

	/* TODO: move in dedicated function */
	if strict {
		mimeTypeFound := false
		for _, entry := range zipreader.File {
			if entry.Name == MIMETYPE_FILE {
				mimeTypeFound = true
				/* check content: should be MIMETYPE_VALUE */
				if reader, err = entry.Open(); err != nil {
					return nil, fmt.Errorf("Failed to open %s", MIMETYPE_FILE)
				}
				buf := make([]byte, entry.UncompressedSize64)
				if _, err := io.ReadFull(reader, buf); err != nil {
					return nil, fmt.Errorf("Failed to read %s", MIMETYPE_FILE)
				}
				buf = bytes.TrimSuffix(buf, []byte("\x0d\x0a"))
				buf = bytes.TrimSuffix(buf, []byte("\x0a"))
				if string(buf) != MIMETYPE_VALUE {
					fmt.Fprintln(os.Stderr, file, "mimetype doesn't contain %s", MIMETYPE_VALUE)
					return nil, fmt.Errorf("Invalid content of %s", MIMETYPE_FILE)
				}
			}
		}
		if !mimeTypeFound {
			return nil, fmt.Errorf("%s is missing", MIMETYPE_FILE)
		}
	}

	for _, entry := range zipreader.File {
		if entry.Name == CONTAINER_XML {
			if reader, err = entry.Open(); err != nil {
				return nil, err
			}
			if opf, err = parseMetaInf(reader, entry.UncompressedSize64); err != nil {
				return nil, err
			}
			break
		}
	}

	for _, entry := range zipreader.File {
		if entry.Name != opf {
			continue
		}
		if reader, err = entry.Open(); err != nil {
			return nil, err
		}

		/* TODO: manage UTF-16 XML*/
		parser := xml.NewDecoder(reader)

		for {
			tok, err := parser.Token()
			if err == io.EOF {
				break
			} else if err != nil {
				return nil, err
			}
			switch tok.(type) {
			default:
			case xml.StartElement:
				elem := tok.(xml.StartElement)
				if elem.Name.Space == DCELEMENTS_SPACE {
					switch elem.Name.Local {
					case "creator":
						/* FIXME: we should check attributes only if not set */
						/* There is several cases:
						   - no attribute at all and Author not already set, use it
						   - no attribute at all but Author is set, skip it
						   - attribute is set, but role is not aut, skip it
						   - attribute is set with role is aut, Author is unset, use it
						   - attribute is set with role is aut, but Author is already set, what to do ?
						*/
						/*
						   for _, entry := range elem.Attr {
						       if entry.Name.Space == OPF_SPACE && entry.Name.Local == "role" && entry.Value == "aut" {
						           fmt.Println(entry.Name)
						       }
						   }
						*/
						if len(book.Author) == 0 {
							field = &book.Author
						}
					case "title":
						field = &book.Title
					case "language":
						/* TODO */
					case "publisher":
						field = &book.Publisher
					case "subject":
						field2 = &book.Tags
					}
				}
			case xml.EndElement:
				field = nil
				field2 = nil
			case xml.CharData:
				elem := tok.(xml.CharData)
				if field != nil {
					*field = string(elem)
				} else if field2 != nil {
					*field2 = append(*field2, string(elem))
				}
			}
		}

		opf_file = true
		break
	}

	if !opf_file {
		return nil, fmt.Errorf("OPF file not found")
	}

	if len(book.Author) == 0 || len(book.Title) == 0 {
		return nil, fmt.Errorf("Author or Title not found")
	}

	/* compute Id for elastic from path to avoid adding same book several times when reparsing */
	book.id = fmt.Sprintf("%x", md5.Sum([]byte(book.Filename)))

	return book, nil
}

func parseDir(root string) []string {
	var result []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("%s => %s\n", path, err)
		}
		if !info.IsDir() && strings.HasSuffix(path, ".epub") {
			result = append(result, path)
		}
		return nil
	})
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return result
}

func parseCmdLine(args []string) *Opts {
	opts := new(Opts)

	params := new(flag.FlagSet)
	params.Init("goepubcheck", flag.ContinueOnError)

	params.StringVar(&opts.elastic, "elastic", "", "URL of Elastic instance")
	params.IntVar(&opts.elastic_bulk_size, "elastic-bulk-size", 10, "number of books for bulk size")
	params.BoolVar(&opts.help, "help", false, "Show help")
	params.BoolVar(&opts.strict, "strict", false, "Check mimetype validity")
	params.BoolVar(&opts.check, "check", false, "Check ZIP for corruption")
	params.BoolVar(&opts.recurse, "recurse", false, "Interpret PATH as directory")
	params.BoolVar(&opts.quiet, "quiet", false, "Quiet mode, only print errors")
	params.BoolVar(&opts.csv, "csv", false, "Turn on CSV output")
	params.BoolVar(&opts.tags, "tag", false, "Extract tags if available")

	if err := params.Parse(args); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if opts.help {
		params.PrintDefaults()
		os.Exit(1)
	}
	opts.nargs = params.Args()

	if len(opts.nargs) == 0 {
		fmt.Println("No filenames, aborting")
		os.Exit(1)
	}

	return opts
}

func initCsv(opts *Opts) *csv.Writer {
	writer := csv.NewWriter(os.Stdout)
	writer.Comma = ';'
	fields := []string{"author", "title", "publisher", "filename"}
	if opts.tags {
		fields = append(fields, "tags")
	}
	if err := writer.Write(fields); err != nil {
		fmt.Fprintln(os.Stderr, "Error encountered while write csv file", err)
		os.Exit(1)
	}
	return writer
}

func checkBook(opts *Opts, file string) (*Book, error) {
	if opts.check {
		_, err := checkZip(file, opts.strict)
		if err != nil {
			return nil, err
		}
	}

	/* read title and author from */
	var book *Book
	var err error
	if book, err = parseXml(file, opts.strict); err != nil {
		return nil, err
	}
	if push != nil {
		if err = push.push(book); err != nil {
			fmt.Printf("%s\n", err)
		}
	}

	if !opts.quiet {
		if opts.csv {
			record := []string{
				book.Author, book.Title, book.Publisher, book.Filename,
			}
			if opts.tags {
				record = append(record, strings.Join(book.Tags, "/"))
			}
			if err := csvWriter.Write(record); err != nil {
				fmt.Fprintln(os.Stderr, "Error encountered while write csv file", err)
				os.Exit(1)
			}
		} else {
			fmt.Printf("%s - %s [%s]\n", book.Author, book.Title, book.Publisher)
		}
	}
	return book, nil
}

func main() {
	opts := parseCmdLine(os.Args[1:])

	var results []string
	if opts.recurse {
		for _, entry := range opts.nargs {
			results = append(results, parseDir(entry)...)
		}
	} else {
		results = opts.nargs
	}
	if !opts.csv {
		fmt.Printf("Got %d files\n", len(results))
	}

	if len(opts.elastic) > 0 {
		push = new(elastic)
		if err := push.init(opts.elastic); err != nil {
			fmt.Fprintln(os.Stderr, "Error encountered while initializing Elastic Search", err)
			os.Exit(1)
		}
		push.setBulkSize(opts.elastic_bulk_size)
		push.start()
	}
	if opts.csv {
		csvWriter = initCsv(opts)
		defer csvWriter.Flush()
	}

	count := 0
	good := 0
	for _, file := range results {
		count += 1

		if _, err := checkBook(opts, file); err != nil {
			fmt.Printf("[KO] %s: %s\n", file, err)
		} else {
			good += 1
		}
	}
	if !opts.csv {
		fmt.Printf("Books %d / Good: %d\n", count, good)
	}
	if push != nil {
		push.stop()
	}
}
