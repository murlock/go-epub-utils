
epubcheck is a small utility to check EPUB:
- is a valid ZIP file
- has proper metadata author and title
- populate an Elastic Search

Usage:
  -check
        Check ZIP for corruption
  -elastic string
        URL of Elastic instance
  -help
        Show help
  -quiet
        Quiet mode, only print errors
  -recurse
        Interpret PATH as directory

How to test CI locally:

- `gitlab-runner exec docker build-my-project`
